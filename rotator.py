"""
This Module contains a wrapper class for the Hamlib rotator class
since that is so poorly documented.
Requires Python>=3.5
"""

import Hamlib
import serial


class RotatorError(Exception):
    def __init__(self, status):
        self.args = status


class Rotator:
    """
    Easycomm3 Rotator class for easier interaction with the
    Hamlib library. Documented Wrapper for the Hamlib.Rot() class.

    Public functions:
    get_rotator()

    open()
    is_open()
    close()

    get_position()
    set_position(azimuth, elevation)

    park()

    reset()
    reboot()

    get_status()
    get_error()
    get_conf(*args)

    Fields:
    rotator -- Hamlib rotator objects
    error_status -- Hamlib rotator error status
    """
    def __init__(self,
                 serial_path="/dev/ttyUSB0",
                 baudrate=19200,
                 min_az=-540,
                 max_az=540,
                 min_el=-10,
                 max_el=190,
                 timeout=200,
                 debug=Hamlib.RIG_DEBUG_NONE,
                 dummy=False):
        """
        Keyword Arguments:
        serial_path -- Path to the serial port of the rotator
                       (Default: "/dev/ttyUSB0")
        baudrate -- baudrate for serial communication (Default: 19200)
        min_az -- minium azimuth angle (Default: -540)
        max_az -- maximum azimuth angle (Default: 540)
        min_el -- minimum elevation angle (Default: -10)
        max_el -- maximum elevation angle (Default: 190)
        timeout -- timeout for serial operations in ms (Default: 200)
        debug -- Hamlib debug verbosity level (Default: Hamlib.RIG_DEBUG_NONE)
        dummy -- wether to use the Hamlib.ROT_MODEL_DUMMY
                 (for testing without Hardware Rotator)
        """
        Hamlib.rig_set_debug(debug)
        self.__dummy = dummy

        if dummy:
            rot_model = Hamlib.ROT_MODEL_DUMMY
        else:
            rot_model = Hamlib.ROT_MODEL_EASYCOMM3

        self.rotator = Hamlib.Rot(rot_model)

        if not dummy:
            self.rotator.set_conf("rot_pathname", serial_path)
            self.rotator.set_conf("serial_speed", str(baudrate))
            self.rotator.set_conf("timeout", str(timeout))

        self.rotator.set_conf("min_az", str(min_az))
        self.rotator.set_conf("max_az", str(max_az))
        self.rotator.set_conf("min_el", str(min_el))
        self.rotator.set_conf("max_el", str(max_el))
        self.error_status = self.rotator.error_status

        self.ser = serial.Serial(serial_path, baudrate=baudrate)

    def __get_error_status(self):
        self.error_status = self.rotator.error_status

    def open(self):
        """Opens the rotator serial port for communication"""
        self.rotator.open()

    def is_open(self):
        """Returns the status of the serial port. 1 is open, 0 is closed"""
        return self.rotator.state.comm_state

    def close(self):
        """Closes the rotator serial port from communication"""
        self.rotator.close()

    def get_rotator(self):
        """Returns the Hamlib.Rot() Easycomm3 rotator object."""
        return self.rotator

    def get_position(self):
        """
        Returns the Azimuth and Elevation of the Rotator.

        Returns:
        [az,el] -- list of int
        """
        position = self.rotator.get_position()

        self.__get_error_status()

        if self.rotator.error_status < 0:
            raise RotatorError(Hamlib.rigerror(self.rotator.error_status))

        return position

    def set_position(self, azimuth, elevation):
        """
        Set the target azimuth and elevation for the rotator.
        Throws RotatorError

        Keyword Arguments:
        azimuth -- target azimuth angle
        elevation -- target elevation angle
        """

        self.rotator.set_position(azimuth, elevation)

        self.__get_error_status()

        if self.rotator.error_status < 0:
            raise RotatorError(Hamlib.rigerror(self.rotator.error_status))

    def park(self):
        """Return the rotator to the home position"""
        self.rotator.park()

        self.__get_error_status()

        if self.rotator.error_status < 0:
            raise RotatorError(Hamlib.rigerror(self.rotator.error_status))

    def stop(self):
        """Stop the rotator"""
        self.rotator.stop()

        self.__get_error_status()

        if self.rotator.error_status < 0:
            raise RotatorError(Hamlib.rigerror(self.rotator.error_status))

    def reset(self):
        """Reset the rotator into homing mode"""
        self.rotator.reset(Hamlib.ROT_RESET_ALL)

        self.__get_error_status()

        if self.rotator.error_status < 0:
            raise RotatorError(Hamlib.rigerror(self.rotator.error_status))

    def get_status(self):
        """
        Returns the contents of the status register of the rotator.

        WARNING: For some reason calling this function sometimes blocks the
        serial communication
        """
        self.rotator.open()

        status = self.rotator.get_conf(3)

        self.__get_error_status()

        if self.rotator.error_status < 0:
            raise RotatorError(Hamlib.rigerror(self.rotator.error_status))

        self.rotator.close()
        return status

    def get_error(self):
        """
        Returns the contents of the error register of the rotator

        WARNING: For some reason calling this function sometimes blocks the
        serial communication
        """

        error = self.rotator.get_conf(4)

        self.__get_error_status()

        if self.rotator.error_status < 0:
            raise RotatorError(Hamlib.rigerror(self.rotator.error_status))

        return error

    def get_conf(self, *args):
        """
        Get Hamlib Configuration data or one of the rotator registers 1-9
        WARNING: Calling the rotator registers somtimes blocks serial
        communication.
        """

        conf = self.rotator.get_conf(*args)

        self.__get_error_status()

        if self.rotator.error_status < 0:
            raise RotatorError(Hamlib.rigerror(self.rotator.error_status))

        return conf

    def reboot(self):
        """
        Reboot the Microcontroller.
        WARNING: This is a custom command on the SatNOGS rotator firmware
        and might not work on all Rotators
        """
        if self.__dummy:
            return

        self.ser.rts = False
        self.rotator.close()

        if not self.ser.is_open:
            self.ser.open()

        try:
            self.ser.write('RB\n'.encode('ASCII'))
            self.rotator.open()
        except serial.SerialException as exce:
            print(exce)
